# 24H-INFO-WEB

Quelques infos pour le développement du site : 

- Une vue extends AbstractView et définit la méthode render() pour **retourner** le contenu du cors de la page.
Pour appeler la vue, dans le contrôleur, on fait echo (new NomDeLaVue())->renderAll(tableau de données); (le tableau étant facultatif). On peut ainsi utiliser dans la vue $this->data pour accéder aux données.
**Attention** : il ne faut pas redéfinir s'occupe de la méthode renderAll et de la création de $this->data, tout est géré dans la classe abstraite.

- Pour afficher des messages d'alertes (erreur de formulaire par exemple), deux possibilités : ```Alerte::create('Message à afficher', Alerte::TYPE_ERREUR)```  (qui enregistre l'erreur) ou ```Alerte::createThenRedirect('message', Alerte::TYPE, 'route ou on redirige', [params facultatifs de la route]);``` (qui redirige puis affiche l'erreur).
Les 4 types d'alertes sont : Alerte::SUCCESS, Alerte::WARNING, Alerte::INFO, Alerte::ERROR.

- Pour la gestion des utilisateurs, c'est très simple :

        - Utilisateur::estConnecte() permet de savoir si l'utilisateur courant est connecté.
        - Utilisateur::getUser() permet de récupérer un utilisateur avec toutes ses données 
        (on peut donc faire par exemple Utilisateur::getUser()->id sans problème)
        - Utilisateur::hasAccess(Utilisateur::TYPE_ACCESS) pour voir si un utilisateur a tel ou tel acces
        Pour le moment les types sont : Utilisateur::USER et Utilisateur::ADMIN (en sachant qu'un admin est un user)
        Les rangs sont extensibles (c'est un table dans la bdd et une liste de constantes dans la classe Utilisateur)