-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Sam 26 Mai 2018 à 05:13
-- Version du serveur :  5.7.22-0ubuntu0.16.04.1
-- Version de PHP :  7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `24h-iut-info`
--

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `id` int(10) NOT NULL,
  `text` varchar(500) DEFAULT NULL,
  `comes` tinyint(4) NOT NULL,
  `userId` int(10) NOT NULL,
  `eventId` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

CREATE TABLE `event` (
  `id` int(10) NOT NULL,
  `public` tinyint(4) NOT NULL DEFAULT '0',
  `title` varchar(200) NOT NULL,
  `descr` varchar(500) NOT NULL,
  `startHour` datetime NOT NULL,
  `stopHour` datetime NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `token` varchar(100) NOT NULL,
  `userId` int(11) NOT NULL,
  `event_before` varchar(256) DEFAULT NULL,
  `event_after` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `event`
--

INSERT INTO `event` (`id`, `public`, `title`, `descr`, `startHour`, `stopHour`, `lat`, `lng`, `token`, `userId`, `event_before`, `event_after`) VALUES
(1, 0, 'Randonnée', 'Petite rando du dimanche', '2018-05-26 00:00:00', '2018-05-26 00:00:00', 48.684391, 6.184960, '1', 8, NULL, NULL),
(2, 1, 'test', 'test', '2019-05-12 00:00:00', '2019-05-12 10:00:00', 0.000000, 0.000000, '1d8e9264f0f30d855fe26a3d', 9, 'test', '');

-- --------------------------------------------------------

--
-- Structure de la table `rank`
--

CREATE TABLE `rank` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `subranks` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `rank`
--

INSERT INTO `rank` (`id`, `name`, `subranks`) VALUES
(1, 'Utilisateur', NULL),
(2, 'Administrateur', 'a:1:{i:0;i:1;}');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(256) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `rank_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `created_at`, `updated_at`, `rank_id`) VALUES
(8, 'jeremy', '$2y$10$xSwqtdgc8AVtUkxdyTxbneoqlMw0kKVDlD9WsLbFogGoGaiUwIEeu', 'azertyuiop@yopmail.com', '2018-05-26 01:06:56', '2018-05-26 01:06:56', 1),
(9, 'test', '$2y$10$2QGMjL0gUE5M2lay2P/DBehYF5UUFIEh44T4pNdpb2/QRyAhg1GkK', 'test@test.fr', '2018-05-26 05:10:14', '2018-05-26 05:10:14', 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_id_comment` (`userId`),
  ADD KEY `fk_event_id_comment` (`eventId`);

--
-- Index pour la table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_id` (`userId`);

--
-- Index pour la table `rank`
--
ALTER TABLE `rank`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `rank`
--
ALTER TABLE `rank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `fk_event_id_comment` FOREIGN KEY (`eventId`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_id_comment` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
