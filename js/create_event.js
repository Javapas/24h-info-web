window.addEventListener("load", ()=> {
    document.querySelector("#form").addEventListener("submit", () => {

        let dateDeb = $("#dateDebut").val();
        let dateFin = $("#dateFin").val();
        let heureDeb = parseInt($("#heureDebut").val());
        let heureFin = parseInt($("#heureFin").val());
        if(dateFin<dateDeb || (dateDeb==dateFin && heureFin<heureDeb)) {
            event.preventDefault();
            let erreur = $("<div>") ;
            erreur.text("Erreur : la date de fin ne peut pas être antérieure à la date de début.") ;
            erreur.addClass("alert") ;
            erreur.addClass("alert-danger") ;
            erreur.css("margin-top", "10px");
            if(document.querySelector('div[class^=alert]')==null)
                $("#form").prepend(erreur);
        }
    });
}) ;