window.addEventListener("load", ()=> {
    document.querySelector("#form").addEventListener("submit", () => {
        event.preventDefault();
        let erreur = document.createElement("div") ;
        erreur.appendChild(document.createTextNode("Votre mail a bien été envoyé.")) ;
        erreur.classList.add("alert") ;
        erreur.classList.add("alert-success") ;
        erreur.style.marginTop = "10px";
        if(document.querySelector('div[class^=alert]')==null)
            document.querySelector("#contact-form").appendChild(erreur);
    });
})