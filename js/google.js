/**
* Fonction executee lors de la saisie d'une donnée dans la barre du formulaire de création d'évent
*/
function formlieu (e) {
  //trouverUneAdresse(e.originalTarget.value,'','');
  var adresse_saisie=document.getElementById('place').value;

  //console.log(adresse_saisie);
  trouverUneAdresse(adresse_saisie,'','');
}


var map=null;
var latitude=0;
var longitude=0;
var adress="";
var home = new google.maps.Marker( { title:'Home', position: new google.maps.LatLng(46,5)} );



initialiserMap(new google.maps.LatLng(30,10),15,'map');

// requetes test
trouverUneAdresse('11 avenue daumesnil',75012,'Paris');


var cur_city_user="";
var cur_cp_user="";
var cur_street_user="";

/**
 * Geocodeur utilisé pour l'adresse de l'utilisateur.
 */
var geocoderUser = null;
/**
 * Geocodeur utilisé pour l'adresse du pro.
 */
var geocoderPro = null;

/**
* trouver une adresse à partir de coordonnées
*/
function trouverUneAdresseAvecCoord(lati,lngi) {
  geocoder = new google.maps.Geocoder();
	// adresse complete
  var latlng = {lat: lati , lng: lngi};
  var geocoder_request =
  {
     location: latlng
  }
  // on récupère la latitude et longitude de la position de l'utilisateur
  var result = {};
  geocoder.geocode( geocoder_request, function(results, status) {
      // on check le status de retour
       if (status == google.maps.GeocoderStatus.OK) {
         // on stock les données dans result[]
           adress = results[1].formatted_address;
           result= adress;

		  // console.log('OK',result);
       } else {
         //result['message'] = "Error, Request denied:  " + status;
         result=null;
		 //console.log('pas ok');
       }
		//console.log(result);
		positionnerUnMarqueur(result);

    });
}
/**
* trouver une position à partir d'une adresse
*/
function trouverUneAdresse(rue,cp,ville){
	geocoderUser = new google.maps.Geocoder();
	// adresse complete
  var adresse = rue + " " + cp + " " + ville + " ";
  var geocoder_request =
  {
     address: adresse
  }
  // on récupère la latitude et longitude de la position de l'utilisateur
  var result = {};
  geocoderUser.geocode( geocoder_request, function(results, status) {
      // on check le status de retour
       if (status == google.maps.GeocoderStatus.OK) {
         // on stock les données dans result[]
           let lat = results[0].geometry.location.lat();
           let lng = results[0].geometry.location.lng();
           latitude=lat;
           longitude=lng;
           result= new google.maps.LatLng(lat,lng);

		   //console.log('OK',result);
       } else {
         //result['message'] = "Error, Request denied:  " + status;
         result=null;
		 //console.log('pas ok');
       }
		//console.log(result);
		positionnerUnMarqueur(result);

    });
}

/**
* Envoie une requête ajax vers un fichier php pour traiter les données de la map
*/
function transfertToPhp(lat,lng) {
  if(lat!=0 && lng !=0){



    var input_lat = document.createElement("input");
    input_lat.setAttribute("type", "hidden");
    input_lat.setAttribute("name", "lat");

    input_lat.setAttribute("value", latitude);

    //append to form element that you want .
    document.getElementById("form").appendChild(input_lat);

    var input_lng = document.createElement("input");
    input_lng.setAttribute("type", "hidden");
    input_lng.setAttribute("name", "lng");

    input_lng.setAttribute("value", longitude);

    //append to form element that you want .
    document.getElementById("form").appendChild(input_lng);
}
}
/**
* Positionner un marqueur sur la position en paramètre
* position : object Latlng
*/
function positionnerUnMarqueur(position){
	//console.log(position.lat(),position.lng());

  home.setMap(null);
	// set un maker home sur la carte
	home = new google.maps.Marker( { title:'Home', position: position} );
  //console.log(home);

	home.setMap(map);

	centrerSurUnePosition(position);
}

function centrerSurUnePosition(position){
	map.setCenter(position);
	console.log('changement de centre');
}

/**
* créer un itinéraire entre 2 positions
*/
function itineraire() {
/*
  start_iti, end_iti, param_carte )
 {
   if ( direction != null )
 	{
    // supprime les itinéraires déjà tracés sur la map
    direction.setMap(null);
		direction = null;
  }
  if (direction == null)
	{
    direction = new google.maps.DirectionsRenderer({
      map   : map
    });
  }

  // tinéraire via deux adresses
  if ( start_iti.type_geoc == 0 && end_iti.type_geoc == 0 )
  {
    var aa = start_iti.adr + ' ' + start_iti.cp + ' ' + start_iti.ville;
		var bb = end_iti.adr + ' ' + end_iti.cp + ' ' + end_iti.ville;

    var request = {
            origin      : aa,
            destination : bb,
            travelMode  : google.maps.DirectionsTravelMode.DRIVING // Type de transport
    }
    directionsService = new google.maps.DirectionsService(); // Service de calcul d'itinéraire
    directionsService.route(request, function(response, status){ // Envoie de la requête pour calculer le parcours
    if(status == google.maps.DirectionsStatus.OK){
          direction.setDirections(response); // Trace l'itinéraire sur la carte et les différentes étapes du parcours
          //callBackRouting(start_iti, end_iti, param_carte);
        }
    });
    //direction.load("from: " + aa + " to: "+ bb,  { getPolyline:true,getSteps:false, "locale": "fr" });
	}
	//itinéraire via deux paires de coordonnées.
	else if ( start_iti.type_geoc == 1 && end_iti.type_geoc == 1 )
	{
		//direction.loadFromWaypoints ( [new GLatLng(start_iti.lat,start_iti.lng), new GLatLng(end_iti.lat,end_iti.lng)], {getPolyline:true,getSteps:false} );
    var aa = new google.maps.LatLng(start_iti.lat,start_iti.lng);
		var bb = new google.maps.LatLng(end_iti.lat,end_iti.lng);

    var request = {
            origin      : aa,
            destination : bb,
            travelMode  : google.maps.DirectionsTravelMode.DRIVING // Type de transport
    }
    directionsService = new google.maps.DirectionsService(); // Service de calcul d'itinéraire
    directionsService.route(request, function(response, status){ // Envoie de la requête pour calculer le parcours
    if(status == google.maps.DirectionsStatus.OK){
          direction.setDirections(response); // Trace l'itinéraire sur la carte et les différentes étapes du parcours
          //callBackRouting(start_iti, end_iti, param_carte);
        }
    });

  }
	//Les types sont disparatres
  else {
  }
  */
}

/**
* Initialise la map
*/
function initialiserMap(center,zoom,balise_html){
    map = new google.maps.Map(document.getElementById(balise_html), {
        center: {lat: center.lat() , lng: center.lng() },
        zoom: zoom
    });
    google.maps.event.addListener(map, 'click', function(event) {
      home.setMap(null);
      positionnerUnMarqueur(event.latLng);
    });

}

/**
* fonction a appeler pour afficher la map dans la description d'un event
*/
function affichageCarteEvent(lat,lng) {
  //setTimeout(function () {
    positionnerUnMarqueur(new google.maps.LatLng(lat,lng));
    home=null;
  //},4000);

}
