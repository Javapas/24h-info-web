<?php

namespace siteWeb\Modeles;


class Rank extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'rank';
    protected $primaryKey = 'id';
    public $timestamps = false;

}
