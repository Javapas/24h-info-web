<?php

namespace siteWeb\Modeles;


class Users extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'users';
    protected $primaryKey = 'id';
    //public $timestamps = false;

    public function rank(){
        return $this->belongsTo('\siteWeb\Modeles\Rank', 'rank_id');
    }

    public function events()
    {
        return $this->hasMany('\siteWeb\Modeles\Event', 'userId', 'id');
    }
}
