<?php
/**
 * Created by PhpStorm.
 * User: oberthe
 * Date: 25/05/2018
 * Time: 22:57
 */

namespace siteWeb\Modeles;


class Comment extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'comment';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('\siteWeb\Modeles\Users', 'userId');
    }
}