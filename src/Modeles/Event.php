<?php
/**
 * Created by PhpStorm.
 * User: oberthe
 * Date: 25/05/2018
 * Time: 22:56
 */

namespace siteWeb\Modeles;


class Event extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'event';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('\siteWeb\Modeles\Users', 'userId');
    }

    public function comments()
    {
        return $this->hasMany('\siteWeb\Modeles\Comment', 'eventId', 'id');
    }

}