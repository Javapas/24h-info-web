<?php
/**
 * Created by PhpStorm.
 * User: oberthe
 * Date: 25/05/2018
 * Time: 23:16
 */

namespace siteWeb\Vues;


class VueEditEvent extends AbstractView
{


    protected function render()
    {
        $racine = BASE_URL;
        $datecourante= (new \DateTime())->format('Y-m-d' );
        $event = $this->data['event'];
        $html = <<<HTML


   <script type="text/javascript" src='$racine/js/google.js'></script>
   <script type="text/javascript" src='$racine/js/create_event.js'></script>

 <!-- Sections -->
 <section class="sections">
 <div class="container">
 <div class="heading text-center">
 <h1>{$event->title}</h1>Modification d'un événement
 <div class="separator2"></div>

 <form action="" method="POST" id="form">
 <div class="row">
 <div class="col-md-12 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="title">Titre</label>
 <input type="text" class="form-control" name="title" id="title" placeholder="" required value="{$event->title}">
 </div>
 </div>

 <div class="col-md-12 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="descr">Description</label>
 <textarea name="descr" class="form-control" id="descr" placeholder="" required>{$event->descr}</textarea>
 </div>
 </div>
 </div>

 <div class="row">
 <div class="col-md-6 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="prev">Lien évenement antérieur <em>(facultatif)</em></label>
 <input name="prev" type="text" class="form-control" id="prev" value="{$event->event_before}">
 </div>
 </div>
 
 <div class="col-md-6 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="next">Lien évenement suivant <em>(facultatif)</em></label>
 <input id="next" type="text" class="form-control" name="next" value="{$event->event_next}">
 </div>


 </div>
 </div>
 <div class="row">
 <div class="col-md-6 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="dateDebut">Date de début</label>
 <input id="dateDebut" type="date" name="dateDebut" required max="2099-12-31" value="
HTML;
    $html .= date('Y-m-d', strtotime($event->startHour));

    $html .= <<<HTML
"/>
 </div>


 <div class="form-group">
 <label for="dateFin">Date de fin</label>
 <input id="dateFin" type="date" name="dateFin" required max="2099-12-31" value="
HTML;
    $html .= date('Y-m-d', strtotime($event->stopHour));
    $html .= <<<HTML
"/>
 </div>


 </div>

 <div class="col-md-6 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="heureDebut">Heure de début</label>
 <select id="heureDebut" name="heureDebut" class="formss">
HTML;
        for($i = 0 ; $i <= 23 ; $i++) {
            $html .= '<option ';
            if (date('H', strtotime($event->startHour)) == $i)
                $html .= 'selected';
            $html .= '>' . $i . '</option>';
        }
        $html.= <<<HTML
 </select>
 <label for="minutesDebut">minutes</label>
 <select id="minutesDebut" name="minutesDebut">
HTML;
    $html.= '<option ';
    if(date('H', strtotime($event->startHour)) == 00)
        $html .= 'selected';
    $html .= '>00</option>
 <option ';
    if(date('i', strtotime($event->startHour)) == 30)
        $html .= 'selected';
    $html .= <<<HTML
>30</option>
 </select>
 </div>


 <label for="heureFin">Heure de fin</label>
 <select id="heureFin" name="heureFin">
HTML;
        for($i = 0 ; $i <= 23 ; $i++) {
            $html .= '<option ';
            if (date('H', strtotime($event->stopHour)) == $i)
                $html .= 'selected';
            $html .= '>' . $i . '</option>';
        }
        $html.= <<<HTML
 </select>
 <label for="minutesFin">minutes</label>
 <select id="minutesFin" name="minutesFin">
HTML;
        $html.= '<option ';
        if(date('H', strtotime($event->stopHour)) == 00)
        $html .= 'selected';
        $html .= '>00</option>
 <option ';
        if(date('i', strtotime($event->stopHour)) == 30)
        $html .= 'selected';
        $html .= <<<HTML
>30</option>
 </select>
 </div>


 <div class="col-md-12 col-sm-12 col-xs-12">
   <div class="form-group">
   <label for="place">Lieu</label>
   <input type="text" class="form-control" name="place" id="place" placeholder="" onkeyup="formlieu(event)">

   <div id="map"></div>
 </div>

 <script type="text/javascript" src='$racine/js/google.js'></script>
 </div>

 <div class="col-md-12 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="public">Cet événement est-il public ?</label>
 <input type="checkbox" name="public" value="Yes" 
HTML;
        if($event->public == 1)
            $html .= 'checked';
    $html .= <<<HTML
 >Oui<br>
 </div>
 </div>
 </div>

 <div class="contact-btn">
 <button id="submit" type="submit" class="btn btn-primary">Modifier</button>
 </div>
 </form>
 </div>
 </div>
 </div>
 </section>

HTML;
        return $html;
    }
}
