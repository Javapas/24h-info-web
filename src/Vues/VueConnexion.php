<?php

namespace siteWeb\Vues;

/**
 * Class VuePageHome
 * Affichage de la page d'accueil du site
 */
class VueConnexion extends AbstractView
{
    public function render()
    {
        $racine = BASE_URL;
        return <<<HTML
        <!-- Sections -->
        <section class="sections">
            <div class="container">
                <div class="heading text-center">
                    <h1>Connexion</h1>
                    <div class="separator2"></div>
 
                            <form action="#" method="POST">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="username">Pseudo</label>
                                            <input type="text" class="form-control" name="username" id="username" placeholder="">
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="pass">Mot de passe</label>
                                            <input type="password" name="pass" class="form-control" id="pass" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="contact-btn">
                                    <button type="submit" class="btn btn-primary">Connexion</button>
                                </div>
                            </form>
                        </div>
          </div>
      </section>
                    

HTML;
    }

}