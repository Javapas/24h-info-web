<?php

namespace siteWeb\Vues;

/**
 * Class VuePageHome
 * Affichage de la page d'accueil du site
 */
class Vue404 extends AbstractView
{
    public function render()
    {
        $racine = BASE_URL;
        return <<<HTML
        <!-- Sections -->
        <section class="sections">
            <div class="container">
                <div class="heading text-center">
                    <h1>Page non trouvée</h1>
                    <div class="separator2"></div>
                    <p>Aïe ! Aïe ! Aïe ! Il n'y a rien à voir par ici... La page recherchée est introuvable... ! Peut-être vous êtes vous trompés dans l'URL ?<br />Si le problème persiste, contactez les développeurs du site ! <em>(ils ne mordent pas !)</em></p>
                </div>
             </div>
         </section>

HTML;
    }

}