<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 25/05/18
 * Time: 10:10
 */

namespace siteWeb\Vues;


use Slim\Slim;

class VueAdminHome extends AbstractView
{
    public function render()
    {
        $racine = BASE_URL;
        $app = Slim::getInstance();
        $listeUtilisateurs = "<ul>";
        foreach($this->data['users'] as $user)
        {
            $listeUtilisateurs .= '<li><a href="'.$app->urlFor('profile',['id' => $user->id]).'">'.$user->username.'</a></li>';
        }
        $listeUtilisateurs .= "</ul>";

        return <<<HTML
        <!-- Sections -->
        <section class="sections">
            <div class="container">
                <div class="heading text-center">
                    <h1>Administration</h1>
                    <div class="separator2"></div>
                    <p>Bienvenue sur la page d'administration ! Voici la liste des utilisateurs :</p>
                    $listeUtilisateurs
                </div>
             </div>
         </section>

HTML;
    }

}