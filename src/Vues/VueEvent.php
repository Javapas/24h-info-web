<?php

namespace siteWeb\Vues;
use siteWeb\Librairies\Utilisateur;
use Slim\Http\Util;
use Slim\Slim;
use siteWeb\Modeles;

/**
 * Class VuePageHome
 * Affichage de la page d'accueil du site
 */
class VueEvent extends AbstractView
{
    function unix_timestamp($date)
    {
        $date = str_replace(array(' ', ':'), '-', $date);
        $c    = explode('-', $date);
        $c    = array_pad($c, 6, 0);
        array_walk($c, 'intval');
    
        return mktime($c[3], $c[4], $c[5], $c[1], $c[2], $c[0]);
    }

    public function render()
    {
        $app = Slim::getInstance();

        $event = $this->data['event'];
        $prevEvent = isset($event->event_before) ? '<a href="'.$event->event_before.'"> <i class="fa fa-arrow-left"></i> Précédent</a>' : '';
        $nextEvent = isset($event->event_after) ? '<a href="'.$event->event_after.'"> Suivant <i class="fa fa-arrow-right"></i></a>' : '';
        $typeEvent = $event->public ? 'public' : 'privé';
        $dateDeb = date('d/m/Y à H\hi', strtotime($event->startHour));
        $dateEnd = date('d/m/Y à H\hi', strtotime($event->stopHour));
        $liensModifs = '';
        $participants = ['ok' => [], 'nonok' => []];
        $url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        foreach($event->comments as $u)
        {
            if($u->comes == 1)
                $participants['ok'][] = $u->user->username;
            else
                $participants['nonok'][] = $u->user->username;
        }

        $participe = "<ul>";
        foreach($participants['ok'] as $p)
            $participe .= "<li>".$p."</li>";
        $participe .= "</ul>";

        $participePas = "<ul>";
        foreach($participants['nonok'] as $p)
            $participePas .= "<li>".$p."</li>";
        $participePas .= "</ul>";
        if(Utilisateur::hasAccess(Utilisateur::ADMIN) || (Utilisateur::estConnecte() && Utilisateur::getUser()->id == $event->user->id))
        {
            $liensModifs = '<div class="row text-center"><a href="'.$app->urlFor('edit_event', ['id' => $event->token]).'" class="btn btn-primary btn-sm">Editer</a><a href="'.$app->urlFor('delete_event', ['id' => $event->token]).'" class="btn btn-danger btn-sm">Delete</a></div>';
        }

        $form = '';
        $unixTime = $this->unix_timestamp($event->startHour);
        if(Utilisateur::hasAccess(Utilisateur::USER) && !$this->data['hasBooked'])
        {
            $form = '                                <div class="row">
                                    <div class="col-md-12">
                                        <h1 class="text-center">Participation</h1>
</div>
</div>
                                 <form action="'.$app->urlFor('comes_event', ['id' => $event->token]).'" method="POST">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="participate">Participation</label>
                                            <input type="radio" class="form-control" name="participate" id="participate" value="1"> Je participe
                                            <input type="radio" class="form-control" name="participate" id="participate" value="0"> Je reste chez moi
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12 col-xs-12">
<div class="form-group">
                                            <label for="comment">Commentaire <em>(facultatif)</em></label>
                                            <textarea name="comment" class="form-control" id="comment"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="contact-btn">
                                    <button type="submit" class="btn btn-primary">Valider</button>
                                </div>
                            </form>';
        }
        $racine = BASE_URL;
        return <<<HTML
        <!-- Sections -->
        <section class="sections">
            <div class="container">
                <div class="heading text-center">
                    <h1>{$event->title}</h1>
                    <p class="text-center">$prevEvent - $nextEvent</p>
                    <p><div class="fb-share-button" data-href="{$url}" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={urlencode($url)}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Partager</a></div><a class="twitter-share-button"
  href="https://twitter.com/intent/tweet?text={$event->title}">
Tweet</a><form action="{$app->urlFor('share_event', ['id' => $event->token])}"  method="POST"><input type="email" placeholder="email" name="email" /> <button>Partager par mail</button> </form></p>
                    <div class="separator2"></div>

                                $liensModifs
                           
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <p>Evénement organisé par {$event->user->username} (<a href="mailto:{$event->user->email}">{$event->user->email}</a>)</p>
                                        <p>{$event->descr}<br /><em>Cet évenement est $typeEvent</em></p>
                                        <p><strong>Début :</strong> {$dateDeb}</p>
                                        <p><strong>Fin :</strong> {$dateEnd}</p>
                                        <h3>Météo prévue ce jour-là</h3>
                                        <p id="localization"></p>
                                        <p id="meteo-title"></p>
                                        <!--<img src="" id="meteo-icon">-->
                                        <canvas id="meteo-icon" width="128" height="128"></canvas>
                                        <p id="meteo-temperature"></p>
                                    </div>

                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div id="map"></div>
                                              	<script type="text/javascript" src='$racine/js/google.js'></script>

                                        <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <h4>Participants</h4>
                                            $participe
                                  </div>
                                 
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                            <h4>Absents</h4>
                                            $participePas
                                  </div>
</div>
                                    </div>
                                </div>

$form
                        </div>
          </div>
      </section>
<script src="$racine/js/skycons.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($){
        var skycons = new Skycons({"color": "black"});
        $.ajax({
            type: "GET",
            url: "https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/192464fcd77a5fbec54bca680221a3f0/{$event->lat},{$event->lng},{$unixTime}"
        }).done(function (data) {
            console.log(data);
            $("#meteo-title").text(data.currently.summary);
            //$("#meteo-icon").attr('src', '../../images/meteo/'+data.data[0].weather.icon+'.png');
            skycons.add("meteo-icon", data.currently.icon);
            $("#meteo-temperature").text((parseFloat(data.currently.temperature-32)/1.8).toFixed(1)+"°C");
        });
        // start animation!
        skycons.play();
    });
        affichageCarteEvent({$event->lat},{$event->lng});
</script>

HTML;
    }

}