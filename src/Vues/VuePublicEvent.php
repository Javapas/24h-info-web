<?php
/**
 * Created by PhpStorm.
 * User: oberthe
 * Date: 26/05/2018
 * Time: 03:08
 */

namespace siteWeb\Vues;


use siteWeb\Modeles\Event;
use Slim\Slim;

class VuePublicEvent extends AbstractView
{

    protected function render()
    {
        $racine = BASE_URL;
        $html = <<<HTML
        <script src="$racine/js/searchBar.js"></script>
<section id="portfolio" class="portfolio lightbg sections">
            <div class="container">
                <div class="heading text-center">
                    <h1>Evénements publiques</h1>
                    <div class="separator"></div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="input-group searchbar">
                               <input type="text" class="form-control" name="barrerecherche" id="barrerecherche" maxlength="25" autocomplete="off" onkeyup="searchFunction()" placeholder="Rechercher un événement, une date, un utilisateur..." />
                          </div>
                     </div>
                    <div class="main_portfolio whitebackground public-event" id="liste">
                        <div class="portffolio_content text-center">

                            <div class="portffolio_content_deteals">
                                <div class="portfolio-one">

HTML;

        $events = Event::where("public", "=", "1")->get();
        $app = Slim::getInstance();
        if(count($events)==0) {
            $html.= <<<HTML
    <div>Aucun événement public n'est encore enregistré sur le site. Soyez le premier à en créer un !</div>
   </div>
<a href="{$app->urlFor('create_event')}" class="btn btn-primary">Créer un événement</a>
HTML;
        }
        foreach ($events as $e) {
            $user = $e->user;
            $date = new \DateTime($e["startDate"]);
            $date = $date->format("d/M/Y");
            $html .= <<<HTML
<div class="col-sm-4 col-xs-12 portfolio-item alkali metal " data-category="alkali">
<div class="single_portfolio_img">
    <a href="{$app->urlFor("show_event", array("id"=> $e["token"]))}">
        <h2 class="text-center">{$e["title"]}</h2>
        <div style="padding: 20px">
            <h4 class="text-left">{$e["descr"]}</h4>
            <h4 class="text-left">Evénement organisé par {$user["username"]} le {$date}</h4>
        </div>
    </a>
</div>

</div>
HTML;
        }

        $html.= <<<HTML

                                	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End of portfolio-one Section -->

HTML;

        return $html;
    }
}