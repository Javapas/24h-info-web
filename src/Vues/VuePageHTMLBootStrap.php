<?php

namespace siteWeb\Vues;
use siteWeb\Librairies\Alerte;
use siteWeb\Librairies\Utilisateur;
use Slim\Http\Util;
use Slim\Slim;

class VuePageHTMLBootStrap
{

    public function __construct()
    {
    }

    public function renderTop()
    {
        $app = Slim::getInstance();
        $racine = BASE_URL;

        $menu = '<li><a href="'.$app->urlFor('root').'">Accueil</a></li>';
        if(Utilisateur::estConnecte())
            $menu.= '<li><a href="'.$app->urlFor('profile_own').'">Profil</a></li>
                    <li><a href="'.$app->urlFor('create_event').'">Créér un rajemblement</a></li>
                    <li><a href="'.$app->urlFor('show_my_event').'">Voir mes événements</a></li>
                    <li><a href="'.$app->urlFor('logout').'">Déconnexion</a></li>';
        else
            $menu .= '<li><a href="'.$app->urlFor('login').'">Connexion</a></li>
                        <li><a href="'.$app->urlFor('register').'">Inscription</a></li>';

        if(Utilisateur::hasAccess(Utilisateur::ADMIN))
            $menu .= '<li><a href="'.$app->urlFor('admin').'">Admin</a></li>';
        return <<<HTML

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Rajemblons nous ! - By Jalu Comment Java ?</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="$racine/css/bootstrap.min.css">


        <!--For Plugins external css-->
        <link rel="stylesheet" href="$racine/css/plugins.css" />
        <link rel="stylesheet" href="$racine/css/lora-web-font.css" />
        <link rel="stylesheet" href="$racine/css/opensans-web-font.css" />
        <link rel="stylesheet" href="$racine/css/magnific-popup.css">

        <!--Theme custom css -->
        <link rel="stylesheet" href="$racine/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="$racine/css/responsive.css" />

        <script src="$racine/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <script
        src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>

        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI0iYea9UWhfOo_L_gYiscBQMD-4Km6_s"
        type="text/javascript"></script>

      	<script type="text/javascript" src='$racine/js/google.js'></script>
    </head>
    <body data-spy="scroll" data-target="#main_navbar">
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v3.0';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>window.twttr = (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0],
    t = window.twttr || {};
  if (d.getElementById(id)) return t;
  js = d.createElement(s);
  js.id = id;
  js.src = "https://platform.twitter.com/widgets.js";
  fjs.parentNode.insertBefore(js, fjs);

  t._e = [];
  t.ready = function(f) {
    t._e.push(f);
  };

  return t;
}(document, "script", "twitter-wjs"));</script>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		<div class='preloader'><div class='loaded'>&nbsp;</div></div>
        <nav class="navbar navbar-default navbar-fixed-top" id="main_navbar">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="$racine/images/logo.png" alt="logo" /></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        $menu
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <!--Home page style-->
        <header id="home" class="home">
            <div class="overlay">
                <div class="container">
                    <div class="row">
                        <div class="home-wrapper">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="home-content text-center">
                                    <h1>Rajemblons-nous ! </h1>
                                    <h4>Organijez et partijipez à des millions d'événements. </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

HTML;
    }

    public function renderAlerts(){
        $html = "";
        foreach(Alerte::getAll() as $type => $tab)
        {
            foreach($tab as $message) {
                $html .= '<div class="fade in alert-dismissible alert alert-';
                switch($type) {
                    case 'success':
                        $html .= 'success';
                        break;
                    case 'info':
                        $html .= 'info';
                        break;
                    case 'error':
                        $html .= 'danger';
                        break;
                    case 'warning':
                        $html .= 'warning';
                        break;
                }
                $html .= '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$message.'</div>';
            }
        }
        Alerte::clear();
        return $html;
    }

    public function renderBottom(){
        $racine = BASE_URL;
        return <<<HTML


        <!--Footer-->
        <footer id="footer" class="footer">
            <div class="container">

                <div class="scroll-top">

                    <div class="scrollup">
                        <i class="fa fa-angle-up"></i>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="copyright">
                            <p>&copy; 2018 - Créé par la Jalu comment Java team dans le cadre des 24h des IUT</p>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="social text-right">
                            <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                            <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                            <a target="_blank" href="#"><i class="fa fa-instagram"></i></a>
                            <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
<script> window.jQuery || document.write('<script src="lib/jquery/jquery.js"><\/script>') </script>

        <script src="$racine/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="$racine/js/vendor/bootstrap.min.js"></script>
        <script src="$racine/js/vendor/isotope.min.js"></script>
        <script src="$racine/js/plugins.js"></script>
         <script src="$racine/js/jquery.magnific-popup.js"></script>
        <script src="$racine/js/main.js"></script>
    </body>
</html>
HTML;
    }
}
