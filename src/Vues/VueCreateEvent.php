<?php
/**
 * Created by PhpStorm.
 * User: oberthe
 * Date: 25/05/2018
 * Time: 23:16
 */

namespace siteWeb\Vues;


class VueCreateEvent extends AbstractView
{


 protected function render()
 {
 $racine = BASE_URL;
 $datecourante= (new \DateTime())->format('Y-m-d' );
 return <<<HTML


   <script type="text/javascript" src='$racine/js/google.js'></script>
   <script type="text/javascript" src='$racine/js/create_event.js'></script>

 <!-- Sections -->
 <section class="sections">
 <div class="container">
 <div class="heading text-center">
 <h1>Création d'un événement</h1>
 <div class="separator2"></div>

 <form action="" method="POST" id="form">
 <div class="row">
 <div class="col-md-12 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="title">Titre</label>
 <input type="text" class="form-control" name="title" id="title" placeholder="" required>
 </div>
 </div>

 <div class="col-md-12 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="descr">Description</label>
 <textarea name="descr" class="form-control" id="descr" placeholder="" required></textarea>
 </div>
 </div>
 </div>

 <div class="row">
 <div class="col-md-6 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="prev">Lien évenement antérieur <em>(facultatif)</em></label>
 <input name="prev" type="text" class="form-control" id="prev">
 </div>
 </div>
 
 <div class="col-md-6 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="next">Lien évenement suivant <em>(facultatif)</em></label>
 <input id="next" type="text" class="form-control" name="next" />
 </div>


 </div>
 </div>
 <div class="row">
 <div class="col-md-6 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="dateDebut">Date de début</label>
 <input id="dateDebut" type="date" name="dateDebut" required min={$datecourante} max="2099-12-31"/>
 </div>


 <div class="form-group">
 <label for="dateFin">Date de fin</label>
 <input id="dateFin" type="date" name="dateFin" required min={$datecourante} max="2099-12-31"/>
 </div>


 </div>

 <div class="col-md-6 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="heureDebut">Heure de début</label>
 <select id="heureDebut" name="heureDebut" class="formss">
 <option>0</option>
 <option>1</option>
 <option>2</option>
 <option>3</option>
 <option>4</option>
 <option>5</option>
 <option>6</option>
 <option>7</option>
 <option>8</option>
 <option>9</option>
 <option>10</option>
 <option>11</option>
 <option>12</option>
 <option>13</option>
 <option>14</option>
 <option>15</option>
 <option>16</option>
 <option>17</option>
 <option>18</option>
 <option>19</option>
 <option>20</option>
 <option>21</option>
 <option>22</option>
 <option>23</option>
 </select>
 <label for="minutesDebut">minutes</label>
 <select id="minutesDebut" name="minutesDebut">
 <option>00</option>
 <option>30</option>
 </select>
 </div>


 <label for="heureFin">Heure de fin</label>
 <select id="heureFin" name="heureFin">
 <option>0</option>
 <option>1</option>
 <option>2</option>
 <option>3</option>
 <option>4</option>
 <option>5</option>
 <option>6</option>
 <option>7</option>
 <option>8</option>
 <option>9</option>
 <option>10</option>
 <option>11</option>
 <option>12</option>
 <option>13</option>
 <option>14</option>
 <option>15</option>
 <option>16</option>
 <option>17</option>
 <option>18</option>
 <option>19</option>
 <option>20</option>
 <option>21</option>
 <option>22</option>
 <option>23</option>
 </select>
 <label for="minutesFin">minutes</label>
 <select id="minutesFin" name="minutesFin">
 <option>00</option>
 <option>30</option>
 </select>
 </div>

 <div class="col-md-12 col-sm-12 col-xs-12">
   <div class="form-group">
   <label for="place">Lieu</label>
   <input type="text" class="form-control" name="place" id="place" placeholder="" onkeyup="formlieu(event)">

   <div id="map"></div>
 </div>

 <script type="text/javascript" src='$racine/js/google.js'></script>
 </div>

 <div class="col-md-12 col-sm-12 col-xs-12">
 <div class="form-group">
 <label for="public">Cet événement est-il public ?</label>
 <input type="checkbox" name="public" value="Yes">Oui<br>
 </div>
 </div>
 </div>

 <div class="contact-btn">
 <button id="submit" type="submit" class="btn btn-primary">Valider</button>
 </div>
 </form>
 </div>
 </div>
 </div>
 </section>

HTML;
 }
}
