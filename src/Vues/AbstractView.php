<?php

namespace siteWeb\Vues;

abstract class AbstractView
{

    protected abstract function render();
    protected $data;

    public function renderAll($data = [])
    {
        $this->data = $data;
        $vueBootstrap = new VuePageHTMLBootStrap();
        $content = $vueBootstrap->renderTop();
        $content .= $vueBootstrap->renderAlerts();
        $content .= $this->render();
        $content .= $vueBootstrap->renderBottom();
        return $content;
    }

}