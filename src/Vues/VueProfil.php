<?php

namespace siteWeb\Vues;
use siteWeb\Librairies\Utilisateur;

/**
 * Class VuePageHome
 * Affichage de la page d'accueil du site
 */
class VueProfil extends AbstractView
{
    public function render()
    {
        $user = $this->data['user'];
        $ranks = $this->data['ranks'];
        $racine = BASE_URL;
        $rank = "";
        if(Utilisateur::hasAccess(Utilisateur::ADMIN))
        {
            $rank = '<div class="row"><div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="rank">Rang d\'accès</label>
                                            <select name="rank" id="rank" class="form-control">';
            foreach($ranks as $r)
            {
               $rank .= '<option value="'.$r->id.'" ';
                if($user->rank_id == $r->id)
                    $rank .= 'selected';
                $rank .= '>'.$r->name.'</option>';
            }
            $rank .= '
                                            </select>
                                        </div>
                                    </div></div>';
        }
        return <<<HTML
        <!-- Sections -->
        <section class="sections">
            <div class="container">
                <div class="heading text-center">
                    <h1>{$user->username}</h1>
                    <div class="separator2"></div>
 
                            <form action="" method="POST">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="username">Pseudo</label>
                                            <input type="text" class="form-control" name="username" id="username" placeholder="" readonly value="{$user->username}">
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="e-mail" name="email" class="form-control" id="email" placeholder="" readonly value="{$user->email}">
                                        </div>
                                    </div>
                                </div>
                                
                                 <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="pass">Nouveau MDP <em>(laissez vide pour ne pas changer)</em></label>
                                            <input type="password" class="form-control" name="pass" id="pass" placeholder="">
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="pass2">Confirmez le MDP <em>(laissez vide pour ne pas changer)</em></label>
                                            <input type="password" class="form-control" name="pass2" id="pass2" placeholder="">
                                        </div>
                                    </div>

                                </div>
                                $rank
                                <div class="contact-btn">
                                    <button type="submit" class="btn btn-primary">Modifier</button>
                                </div>
                            </form>
                        </div>
          </div>
      </section>
                    

HTML;
    }

}