<?php

namespace siteWeb\Vues;
use siteWeb\Modeles\Event;
use Slim\Slim;

/**
 * Class VuePageHome
 * Affichage de la page d'accueil du site 
 */
class VuePageHome extends AbstractView
{
    public function render()
    {
        $racine = BASE_URL;
        $html = <<<HTML
        <!-- Sections -->
        <section id="service" class="service sections">
            <div class="container">
                <div class="heading text-center">
                    <h1>Nos services</h1>
                    <div class="separator"></div>
                </div>
                <!-- Example row of columns -->
                <div class="row">
                    <div class="wrapper">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="service-item text-center">
                                <i class="fa fa-calendar"></i>
                                <h5>Evénement</h5>
                                <div class="separator2"></div>
                                <p>Créez des événements en illimité et partagez les à vos amis ou collègues.</p>
                            </div>
                        </div> 

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="service-item text-center">
                                <i class="fa fa-check"></i>
                                <h5>Participation</h5>
                                <div class="separator2"></div>
                                <p>Participez à des évenements et laissez un commentaire !</p>
                            </div>
                        </div> 

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="service-item text-center">
                                <i class="fa fa-map-marker"></i>
                                <h5>Localisation</h5>
                                <div class="separator2"></div>
                                <p>Aidez-vous de notre module de localisation pour accéder à vos évenements rapidement.</p>
                            </div>
                        </div> 

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="service-item text-center">
                                <i class="fa fa-plus"></i>
                                <h5>Et bien plus !</h5>
                                <div class="separator2"></div>
                                <p>Plein d'autres informations sont à votre disposition tels que la météo prévue.</p>
                            </div>
                        </div> 

                    </div>
                </div>
            </div> <!-- /container -->       
        </section>


        <section id="portfolio" class="portfolio lightbg sections">
            <div class="container">
                <div class="heading text-center">
                    <h1>Evénements publiques</h1>
                    <div class="separator"></div>
                </div>
                <div class="row">
                    <div class="main_portfolio whitebackground">
                        <div class="portffolio_content text-center">

                            <div class="portffolio_content_deteals">
                                <div class="portfolio-one">

HTML;

        $events = Event::where("public", "=", "1")->get();
        $app = Slim::getInstance();
        if(count($events)==0) {
            $html.= <<<HTML
    <div>Aucun événement public n'est encore enregistré sur le site. Soyez le premier à en créer un !</div>
   </div>
<a href="{$app->urlFor('create_event')}" class="btn btn-primary">Créer un événement</a>
HTML;
        }
        else {
            for ($i=0 ; $i<6 && $i<count($events); $i++) {
                $user = $events[$i]->user;
                $date = new \DateTime($events[$i]["startDate"]);
                $date = $date->format("d/m/Y");
                $html .= <<<HTML
<div class="col-sm-4 col-xs-12 portfolio-item alkali metal " data-category="alkali">
    <div class="single_portfolio_img">
        <a href="{$app->urlFor("show_event", array("id"=> $events[$i]["token"]))}">
            <h2 class="text-center">{$events[$i]["title"]}</h2>
            <div style="padding: 20px">
                <h4 class="text-left">{$events[$i]["descr"]}</h4>
                <h4 class="text-left">Evénement organisé par {$user["username"]} le {$date}</h4>
            </div>
        </a>
    </div>

</div>
HTML;

            }
            $html.= <<<HTML
</div>
<a href="{$app->urlFor("show_public_event")}" class="btn btn-primary">Voir plus</a>
HTML;

        }

        $html.= <<<HTML

                                	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End of portfolio-one Section -->

        <!-- Sections -->
        <section id="brand" class="brand sections">
            <div class="container">
                <div class="heading text-center">
                    <h1>Ces partenaires nous font confiance</h1>
                    <div class="separator"></div>
                </div>
                <!-- Example row of columns -->
                <div class="row">
                    <div class="wrapper brand-category ">
                        <div class="brand-item">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="brand-identity">
                                    <img src="$racine/images/brand/1.jpg" alt="brand" />
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="brand-identity">
                                    <img src="$racine/images/brand/2.jpg" alt="brand" />
                                </div>
                            </div>

                            <div class="brand-item col-md-2 col-sm-2 col-xs-12">
                                <div class="brand-identity">
                                    <img src="$racine/images/brand/3.jpg" alt="brand" />
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="brand-identity">
                                    <img src="$racine/images/brand/4.jpg" alt="brand" />
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="brand-brand-identity">
                                    <img src="$racine/images/brand/5.jpg" alt="brand" />
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="brand-identity">
                                    <img src="$racine/images/brand/2.jpg" alt="brand" />
                                </div>
                            </div>
                        </div>
                        <div class="brand-item">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="brand-identity">
                                    <img src="$racine/images/brand/1.jpg" alt="brand" />
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="brand-identity">
                                    <img src="$racine/images/brand/2.jpg" alt="brand" />
                                </div>
                            </div>

                            <div class="brand-item col-md-2 col-sm-2 col-xs-12">
                                <div class="brand-identity">
                                    <img src="$racine/images/brand/3.jpg" alt="brand" />
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="brand-identity">
                                    <img src="$racine/images/brand/4.jpg" alt="brand" />
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="brand-brand-identity">
                                    <img src="$racine/images/brand/5.jpg" alt="brand" />
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="brand-identity">
                                    <img src="$racine/images/brand/2.jpg" alt="brand" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- /container -->       
        </section>

        <!-- Sections -->
        <!--
        <section id="testimonial" class="testimonial lightbg sections">
            <div class="container">
                <div class="heading text-center">
                    <h1>See what others are saying about us</h1>
                    <div class="separator"></div>
                </div>
                <!-- Example row of columns -->
                <!--<div class="row">
                    <div class="col-md-12" data-wow-delay="0.2s">
                        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                            <!-- Bottom Carousel Indicators -->
                            <!--<ol class="carousel-indicators">
                                <li data-target="#quote-carousel" data-slide-to="0" class="active">
                                    <img class="img-responsive " src="$racine/images/team/1.jpg" alt="Team Member">

                                </li>
                                <li data-target="#quote-carousel" data-slide-to="1">
                                    <img class="img-responsive" src="$racine/images/team/2.jpg" alt="Team Member">

                                </li>
                                <li data-target="#quote-carousel" data-slide-to="2">
                                    <img class="img-responsive" src="$racine/images/team/3.jpg" alt="Team Member">

                                </li>
                            </ol>

                            <!-- Carousel Slides / Quotes -->
                           <!-- <div class="carousel-inner text-center margin-top-60">

                                <!-- Quote 1 -->
                               <!-- <div class="item active">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3 details">
                                            <p>" Lorem  ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                                            <small>Linda from <a href="#">example.com</a></small>
                                        </div>
                                    </div>
                                </div>
                                <!-- Quote 2 -->
                                <!--<div class="item">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3 details">

                                            <p>" Lorem  ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                                            <small>Linda from <a href="#">example.com</a></small>
                                        </div>
                                    </div>
                                </div>
                                <!-- Quote 3 -->
                                <!--<div class="item">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3 details">
                                            <p>" Lorem  ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."</p>
                                            <small>Linda from <a href="#">example.com</a></small>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <!-- Carousel Buttons Next/Prev -->
                            <!--<a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>

                </div>
            </div> <!-- /container -->       
        <!--</section>-->

        <!-- Sections -->
        <section id="contact" class="contact">
            <div class="container">
                <!-- Example row of columns -->
                <div class="row">

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="contact-top">
                            <h1>Contactez nous !</h1>
                        </div>
                        <div class="contact-left-info">
                            <h5>Notre adresse</h5>
                            <p>666 rue de la nuit noire, 54 098 Javaville</p>
                        </div>

                        <div class="contact-left-info">
                            <h5>Notre téléphone</h5>
                            <p>+33 666666666</p>
                        </div>

                        <div class="contact-left-info">
                            <h5>Notre email</h5>
                            <p>jalucommentjava@gmail.com</p>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-6 col-xs-12">
                        <!--<div class="contact-top navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right bottom-nav">
                                <li><a href="#home">Home</a></li>
                                <li><a href="#service">Services</a></li>
                                <li><a href="#portfolio">Portfolio</a></li>
                                <li><a href="#testimonial">Testimonials</a></li>
                                <li><a href="#contact">Contact</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->

                        <div class="contact-form" id="contact-form">
                            <form action="" method="POST" id="form">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Sujet</label>
                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Message</label>
                                            <textarea rows="7" class="form-control" id="exampleInputPassword1" placeholder=""></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="contact-btn">
                                    <button type="submit" class="btn btn-primary">Envoyer</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div> <!-- /container -->       
        </section>
        
        <script type="text/javascript" src='$racine/js/envoi_mail.js'></script>

HTML;

        return $html;
    }

}