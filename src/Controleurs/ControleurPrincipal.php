<?php

namespace siteWeb\Controleurs;

use siteWeb\Librairies\Alerte;
use siteWeb\Vues\Vue404;
use siteWeb\Vues\VuePageHome;


/**
 * Class ControleurTheme
 * Controle la page VueRechercheTheme
 * @package blindtest\Controleurs
 */
class ControleurPrincipal
{
    public function home()
    {
        echo (new VuePageHome())->renderAll();
    }

    public function notFound()
    {
        echo (new Vue404())->renderAll();
    }
}
