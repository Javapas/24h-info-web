<?php

namespace siteWeb\Controleurs;


use siteWeb\Librairies\Alerte;
use siteWeb\Librairies\Utilisateur;
use siteWeb\Modeles\Rank;
use siteWeb\Modeles\Users;
use siteWeb\Vues\VueConnexion;
use siteWeb\Vues\VueEventUser;
use siteWeb\Vues\VueInscription;
use siteWeb\Vues\VueProfil;
use Slim\Http\Util;
use Slim\Slim;
use siteWeb\Vues\VuePageHTMLBootStrap;

/**
 * Class ControleurTheme
 * Controle la page VueRechercheTheme
 * @package blindtest\Controleurs
 */
class ControleurUtilisateur
{
    public function afficherConnexion()
    {
        if(Utilisateur::estConnecte())
            Alerte::createThenRedirect('Vous êtes déjà connecté !', Alerte::WARNING, 'root');
        echo (new VueConnexion())->renderAll();
    }

    public function afficherInscription()
    {
        if(Utilisateur::estConnecte())
            Alerte::createThenRedirect('Vous êtes déjà inscrit !', Alerte::WARNING, 'root');
        echo (new VueInscription())->renderAll();
    }

    public function traitementConnexion()
    {
        if(Utilisateur::estConnecte())
            Alerte::createThenRedirect('Vous êtes déjà connecté !', Alerte::WARNING, 'root');

        if(empty($_POST['username']) || empty($_POST['pass']))
            Alerte::createThenRedirect('Tous les champs doivent être remplis', Alerte::WARNING, 'login');;
        $user = Users::where('username','=',$_POST['username'])->first();
        if(!$user)
            Alerte::createThenRedirect('Cet utilisateur n\'existe pas !', Alerte::WARNING, 'login');

        if(!password_verify($_POST['pass'], $user->password))
            Alerte::createThenRedirect('Mot de passe incorrect', Alerte::WARNING, 'login');

        Utilisateur::saveUser($user);
        Alerte::createThenRedirect('Bienvenue '.$user->username.' !', Alerte::SUCCESS, 'root');
    }

    public function traitementInscription()
    {
        if(Utilisateur::estConnecte())
            Alerte::createThenRedirect('Vous êtes déjà inscrit !', Alerte::WARNING, 'root');

        if(empty($_POST['username']) || empty($_POST['pass']) || empty($_POST['pass2']) || empty($_POST['email']))
            Alerte::createThenRedirect('Tous les champs doivent être remplis', Alerte::WARNING, 'register');

        if($_POST['pass'] != $_POST['pass2'])
            Alerte::createThenRedirect('Les deux mots de passe sont différents', Alerte::WARNING, 'register');

        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
            Alerte::createThenRedirect('L\'adresse mail n\'est pas valide !', Alerte::WARNING, 'register');

        $user = Users::where('username','=',$_POST['username'])->orWhere('email', '=', $_POST['email'])->first();
        if($user)
            Alerte::createThenRedirect('Pseudo ou email déjà utilisé', Alerte::WARNING, 'register');

        $user = new Users();
        $user->username = filter_var($_POST['username'], FILTER_SANITIZE_SPECIAL_CHARS);
        $user->password = password_hash($_POST['pass'], PASSWORD_DEFAULT);
        $user->email = $_POST['email'];
        $user->rank_id = 1; // Rang utilisateur de base
        $user->save();
        Alerte::createThenRedirect('Vous êtes bien inscrits ! Vous pouvez maintenant vous connecter !', Alerte::SUCCESS, 'login');

    }

    public function deconnexion()
    {
        Utilisateur::logout();
        Alerte::createThenRedirect('Vous êtes bien déconnecté. Au revoir !', Alerte::SUCCESS, 'root');
    }

    public function affichageProfil($id)
    {
        if(!Utilisateur::estConnecte())
            Alerte::createThenRedirect('Vous devez être connecté pour accéder à cette page', Alerte::WARNING, 'login');

        if($id != 0 && !Utilisateur::hasAccess(Utilisateur::ADMIN) && $id != Utilisateur::getUser()->id)
            Alerte::createThenRedirect('Vous ne pouvez pas accéder à cette page', Alerte::ERROR, 'root');

        $id = ($id == 0) ? Utilisateur::getUser()->id : $id;

        $user = Users::where('id', '=', $id)->first();
        if(!$user)
            Alerte::createThenRedirect('Cet utilisateur n\'existe pas', Alerte::WARNING, 'root');

        $rank = Rank::get();
        echo (new VueProfil())->renderAll(['user' => $user, 'ranks' => $rank]);
    }

    public function traitementProfil($id)
    {
        if(!Utilisateur::estConnecte())
            Alerte::createThenRedirect('Vous devez être connecté pour accéder à cette page', Alerte::WARNING, 'login');

        if($id != 0 && !Utilisateur::hasAccess(Utilisateur::ADMIN) && $id != Utilisateur::getUser()->id)
            Alerte::createThenRedirect('Vous ne pouvez pas accéder à cette page', Alerte::ERROR, 'root');

        if(isset($_POST['pass']) && $_POST['pass'] != $_POST['pass2'])
            Alerte::createThenRedirect('Les deux mots de passe ne sont pas les mêmes', Alerte::WARNING, $id == 0 ? 'profile_own' : 'profile', ['id' => $id]);
        $id = ($id == 0) ? Utilisateur::getUser()->id : $id;
        $user = Users::where('id', '=', $id)->first();

        if(isset($_POST['pass']) && $_POST['pass'] != "")
            $user->password = password_hash($_POST['pass'], PASSWORD_DEFAULT);

        if(Utilisateur::hasAccess(Utilisateur::ADMIN))
            $user->rank_id = $_POST['rank'];

        $user->save();
        Alerte::createThenRedirect('Profil mis à jour avec succès !', Alerte::SUCCESS, 'profile', ['id' => $id]);

    }

    public function showEvents() {
        if(Utilisateur::estConnecte()) {
            echo (new VueEventUser())->renderAll();
        }
        else {
            Alerte::createThenRedirect('Vous devez être connecté pour accéder à cette fonctionnalité !', Alerte::WARNING, 'root');
        }
    }
}
