<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 25/05/18
 * Time: 22:57
 */

namespace siteWeb\Controleurs;


use siteWeb\Librairies\Alerte;
use siteWeb\Librairies\Utilisateur;
use siteWeb\Modeles\Comment;
use siteWeb\Modeles\Event;
use siteWeb\Vues\VueCreateEvent;
use siteWeb\Vues\VueEditEvent;
use siteWeb\Vues\VueEvent;
use siteWeb\Vues\VuePublicEvent;
use Slim\Http\Util;
use Slim\Slim;

class ControleurEvent
{
    public function create()
    {
        if(Utilisateur::estConnecte()) {
            echo (new VueCreateEvent())->renderAll();
        }
        else {
            Alerte::createThenRedirect('Vous devez être connecté pour accéder à cette fonctionnalité !', Alerte::WARNING, 'root');
        }
    }

    public function createValid(){
        var_dump($_POST);
        $title = htmlspecialchars($_POST["title"]);
        $descr = htmlspecialchars($_POST["descr"]);
        $public = 0;
        if(isset($_POST['public']) && $_POST['public']==='Yes') $public=1;
        $event = new Event();
        $event->id=Event::max('id')+1;
        $event->public=$public;
        $event->title= $title;
        $event->descr=$descr;
        $event->startHour=$_POST['dateDebut']." ".$_POST['heureDebut'].":".$_POST['minutesDebut'];
        $event->stopHour=$_POST['dateFin']." ".$_POST['heureFin'].":".$_POST['minutesFin'];
        $event->lat=$_POST['lat'];
        $event->lng=$_POST['lng'];
        $event->token=bin2hex(random_bytes(12));
        $event->userId=Utilisateur::getUser()->id;
        $event->event_before= $_POST['prev'] ?? null;
        $event->event_after = $_POST['next'] ?? null;

        $event->save();
        //$startDate = $_POST["startDate"];
        $app = Slim::getInstance();
        $app->redirectTo("show_event", array("id"=>$event->token));
    }

    public function edit($id)
    {
        if(!Utilisateur::estConnecte())
            Alerte::createThenRedirect('Vous devez être connecté pour accéder à cette page', Alerte::WARNING, "root");

        $event = Event::where('token','=',$id)->first();
        if(!$event)
            Alerte::createThenRedirect('Cet évenement n\'existe pas !', Alerte::WARNING, 'root');

        if(!Utilisateur::hasAccess(Utilisateur::ADMIN) && Utilisateur::getUser()->id != $event->userId)
            Alerte::createThenRedirect('Vous n\'avez pas les accès nécessaires', Alerte::ERROR, 'root');

        echo (new VueEditEvent())->renderAll(['event' => $event]);

    }

    public function editValid($id)
    {
        if(!Utilisateur::estConnecte())
            Alerte::createThenRedirect('Vous devez être connecté pour accéder à cette page', Alerte::WARNING, "root");

        $event = Event::where('token','=',$id)->first();
        if(!$event)
            Alerte::createThenRedirect('Cet évenement n\'existe pas !', Alerte::WARNING, 'root');

        if(!Utilisateur::hasAccess(Utilisateur::ADMIN) && Utilisateur::getUser()->id != $event->userId)
            Alerte::createThenRedirect('Vous n\'avez pas les accès nécessaires', Alerte::ERROR, 'root');

        $title = htmlspecialchars($_POST["title"]);
        $descr = htmlspecialchars($_POST["descr"]);
        $public = 0;
        if(isset($_POST['public']) && $_POST['public']==='Yes') $public=1;
        $event->public=$public;
        $event->title= $title;
        $event->descr=$descr;
        $event->startHour=$_POST['dateDebut']." ".$_POST['heureDebut'].":".$_POST['minutesDebut'];
        $event->stopHour=$_POST['dateFin']." ".$_POST['heureFin'].":".$_POST['minutesFin'];
        $event->lat=0;
        $event->lng=0;
        $event->userId=Utilisateur::getUser()->id;
        $event->event_before= $_POST['prev'] ?? null;
        $event->event_after = $_POST['next'] ?? null;

        $event->save();
        //$startDate = $_POST["startDate"];
        $app = Slim::getInstance();
        Alerte::create('Evenement modifié !', Alerte::SUCCESS);
        $app->redirectTo("show_event", array("id"=>$event->token));
    }

    public function comes($id)
    {
        if(!Utilisateur::estConnecte())
            Alerte::createThenRedirect('Vous devez être connecté pour cela', Alerte::WARNING, 'login');

        $event = Event::where('token', '=', $id)->first();
        if(!$event)
            Alerte::createThenRedirect('Cet evenement n\'existe pas !', Alerte::WARNING, 'root');

        $comm = Comment::where('userId', '=', Utilisateur::getUser()->id)->where('eventId', '=', $event->id)->first();
        if($comm)
            Alerte::createThenRedirect('Vous avez déjà fait cette action...', Alerte::WARNING, 'show_event', ['id' => $id]);

        $comm = new Comment();
        $comm->userId = Utilisateur::getUser()->id;
        $comm->eventId = $event->id;
        $comm->text = isset($_POST['comment']) ? htmlspecialchars($_POST['comment']) : '';
        $comm->comes = isset($_POST['participate']) && ($_POST['participate'] == 0 || $_POST['participate'] == 1) ? $_POST['participate'] : 0;
        $comm->save();

        Alerte::createThenRedirect('Merci d\'avoir reseigné cela ! A bientôt !', Alerte::SUCCESS, 'show_event', ['id' => $id]);

    }

    public function show($id)
    {
        $event = Event::where('token', '=', $id)->first();
        if(!$event)
            Alerte::createThenRedirect('Cet évenement n\'existe pas ou plus... :\'(', Alerte::WARNING, 'root');
        if(Utilisateur::estConnecte())
            $hasBooked = (Comment::where('eventId', '=', $event->id)->where('userId','=',Utilisateur::getUser()->id)->first() != null);
        else
            $hasBooked = false;

        echo (new VueEvent())->renderAll(['event' => $event, 'hasBooked' => $hasBooked]);
    }

    public function delete($id)
    {
        $event = Event::where('token', '=', $id)->first();
        if(!$event)
            Alerte::createThenRedirect('Cet évenement n\'existe pas ou plus... :\'(', Alerte::WARNING, 'root');

        if(!Utilisateur::hasAccess(Utilisateur::ADMIN) && !(Utilisateur::estConnecte() && Utilisateur::getUser()->id == $event->userId))
            Alerte::createThenRedirect('Vous n\'avez pas les accès nécessaire pour supprimer cet évent', Alerte::ERROR, 'root');

        foreach ($event->comments as $c) {
            $c->delete();
        }
        $event->delete();

        Alerte::createThenRedirect('Evenement supprimé avec succès !', Alerte::SUCCESS, 'root');
    }

    public function share($id)
    {
        $event = Event::where('token','=',$id)->first();

        if(empty($_POST['email']) || $event == null || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
            Alerte::createThenRedirect('Envoi impossible', Alerte::ERROR, 'show_event', ['id'=>$id]);

        mail($_POST['email'], 'Evenement '.$event->title, 'Bonjour,\nVoici un évenement qui devrait vous plaire : '.$event->title.' : '."http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
        Alerte::createThenRedirect('Message partagé !', Alerte::SUCCESS, 'show_event', ['id'=>$id]);
    }

    public function showPublic()
    {
        echo (new VuePublicEvent())->renderAll();
    }
}
