<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 25/05/18
 * Time: 10:09
 */

namespace siteWeb\Controleurs;


use siteWeb\Librairies\Alerte;
use siteWeb\Librairies\Utilisateur;
use siteWeb\Modeles\Users;
use siteWeb\Vues\VueAdminHome;

class ControleurAdmin
{

    public function home()
    {
        if(!Utilisateur::hasAccess(Utilisateur::ADMIN))
            Alerte::createThenRedirect('Vous n\'avez pas les droits nécessaires pour accéder à cette page', Alerte::ERROR, 'root');

        $users = Users::get();
        echo (new VueAdminHome())->renderAll(['users' => $users]);
    }
}