<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 24/05/18
 * Time: 23:35
 */

namespace siteWeb\Librairies;


class Utilisateur
{
    // Pour chaque rang en BDD, créer une constante en y associant l'ID du rang
    const USER = 1;
    const ADMIN = 2;

    public static function estConnecte()
    {
        return isset($_SESSION['user-manager']);
    }

    public static function getUser()
    {
        if(self::estConnecte())
            return unserialize($_SESSION['user-manager']);
        return null;
    }

    public static function saveUser($user)
    {
        $_SESSION['user-manager'] = serialize($user);
    }

    public static function logout()
    {
        if(self::estConnecte())
            unset($_SESSION['user-manager']);
    }

    public static function hasAccess($access)
    {
        if(!self::estConnecte())
            return false;

        if(self::getUser()->rank == null)
            return false;

        if(self::getUser()->rank->id == $access)
            return true;

        if(self::getUser()->rank->subranks != null)
            foreach(unserialize(self::getUser()->rank->subranks) as $rank)
                if($rank == $access)
                    return true;
        return false;
    }
}
