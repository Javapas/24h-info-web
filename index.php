<?php
include 'conf/config.php';
error_reporting(E_ALL);
ini_set('display_errors', 'On');
session_start();
/**
 * Importation
 */
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use \siteWeb\Controleurs as c;
use \siteWeb\Vues as v;

/**
 * Base de données
 * Nommer son fichier de configuration de conenxion à la base de données : "dbconf.ini
 */
 
 
 /**
 * MIS EN COMMENTAIRE DE FACON PROVISOIRE (TANT QU'ON A PAS DE BASE)
 */
$db = new DB();
$db->addConnection(parse_ini_file('dbconf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

/**
 * Application Slim
 */
$app = new Slim\Slim();

/**
 * Routes
 */

$app->notFound(function () {
    (new c\ControleurPrincipal())->notFound();
}) ;

/**
 * Routes pour les inscriptions et connexions
 */
$app->get('/',function(){
    (new c\ControleurPrincipal())->home();
})->name('root') ;


$app->get('/login', function(){
    (new c\ControleurUtilisateur())->afficherConnexion();
})->name('login');

$app->get('/register', function(){
    (new c\ControleurUtilisateur())->afficherInscription();
})->name('register');

$app->post('/login', function(){
    (new c\ControleurUtilisateur())->traitementConnexion();
})->name('login_valid');

$app->post('/register', function(){
    (new c\ControleurUtilisateur())->traitementInscription();
})->name('register_valid');

$app->get('/logout', function(){
    (new c\ControleurUtilisateur())->deconnexion();
})->name('logout');

$app->get('/profile/:id', function($id) {
    (new c\ControleurUtilisateur())->affichageProfil($id);
})->name('profile');

$app->get('/profile', function(){
    (new c\ControleurUtilisateur())->affichageProfil(0);
})->name('profile_own');

$app->post('/profile/:id', function($id){
    (new c\ControleurUtilisateur())->traitementProfil($id);
})->name('profile_valid');

$app->post('/profile', function(){
    (new c\ControleurUtilisateur())->traitementProfil(0);
});

$app->get('/admin', function(){
    (new c\ControleurAdmin())->home();
})->name('admin');



$app->get('/event/:id/edit', function($id){
    (new c\ControleurEvent())->edit($id);
})->name('edit_event');

$app->post('/event/:id/edit', function($id){
    (new c\ControleurEvent())->editValid($id);
})->name('edit_event_valid');

$app->get('/event/:id/delete', function($id){
    (new c\ControleurEvent())->delete($id);
})->name('delete_event');

$app->get('/event/create', function(){
    (new c\ControleurEvent())->create();
})->name('create_event');

$app->post('/event/create', function (){
    (new c\ControleurEvent())->createValid();
})->name('create_event_valid');


$app->post('/event/:id/comes', function($id){
    (new c\ControleurEvent())->comes($id);
})->name('comes_event');

$app->get('/event/public/show', function(){
    (new c\ControleurEvent())->showPublic();
})->name('show_public_event');

$app->get('/event/showAll', function(){
    (new c\ControleurUtilisateur())->showEvents();
})->name('show_my_event');

$app->post('/event/:id/share', function($id){
    (new c\ControleurEvent())->share($id);
})->name('share_event');

$app->get('/event/:id', function($id){
    (new c\ControleurEvent())->show($id);
})->name('show_event');

/**
 * Lancement de l'application Slim
 */
$app->run() ;
